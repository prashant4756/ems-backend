package com.hibernate.test;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "employee")
//@JsonRootName("employees")
public class Employee implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name="employeeId")
	private int employeeId;
	@XmlElement(name="employeeName")
	private String employeeName;
	@XmlElement(name="departmentName")
	private String departmentName;
	@XmlElement(name="profession")
	private String profession;
	@XmlElement(name="gender")
	private String gender;
	
	public Employee(){
		
	}
	
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", departmentName="
				+ departmentName + ", profession=" + profession + ", gender=" + gender + "]";
	}

	public Employee(int employee_id, 
			String employee_name,
			String department_name,
			String profession,
			String gender){
		this.employeeId=employee_id;
		this.employeeName=employee_name;
		this.departmentName=department_name;
		this.profession=profession;
		this.gender=gender;
	}
	
	//getter
	public int getEmployeeID(){
		return this.employeeId;
	}
	public String getEmployeeName(){
		return this.employeeName;
	}
	public String getDepartmentName(){
		return this.departmentName;
	}
	public String getProfession(){
		return this.profession;
	}
	public String getGender(){
		return this.gender;
	}
	
	
	//setter
	public void setEmployeeID(int employee_id){
		this.employeeId=employee_id;
	}
	public void setEmployeeName(String employee_name){
		this.employeeName=employee_name;
	}
	public void setDepartmentName(String department_name){
		this.departmentName=department_name;
	}
	public void setProfession(String profession){
		this.profession=profession;
	}
	public void setGender(String gender){
		this.gender=gender;
	}
	
}
