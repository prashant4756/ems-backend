package com.hibernate.test;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/HibernateService")
public class HibernateService {
	   
	   @GET
	   @Path("/allEmployees")
	   @Produces(MediaType.APPLICATION_JSON)
	   public List<Employee> getUsers(){
		  return DBHandler.listEmployees();
		   
	   }
	   
	   @GET
	   @Path("/allEmployees/{employee_id}")
	   @Produces(MediaType.APPLICATION_JSON)
	   public Employee getUserByEmployeeId(@PathParam("employee_id") int employee_id){
		   
		   return DBHandler.getUserByEmployeeId(employee_id);
		   
	   }
	   
	   @POST
	   @Path("/allEmployees")
	   @Produces(MediaType.APPLICATION_JSON)
	   public List<Employee> addEmployee(Employee employee){
		
		System.out.println(employee.getEmployeeName());
		return DBHandler.addEmployee(employee);
		   
	   }
	   
	   @DELETE
	   @Path("/deleteEmployee")
	   @Produces(MediaType.APPLICATION_JSON)
	   public boolean deleteEmployee(Employee employee){
		   System.out.println("inside delete service file"+employee.getEmployeeID());
		   return DBHandler.deleteEmployee(employee.getEmployeeID());
	   }
	   
	   @POST
	   @Path("/updateEmployee")
	   @Produces(MediaType.APPLICATION_JSON)
	   public boolean updateEmployee(Employee employee){
		System.out.println("inside update service file" + employee);
		System.out.println("inside update service file" + employee.getEmployeeID());
		System.out.println("inside update service file" + employee.getEmployeeName());
		System.out.println("inside update service file" + employee.getDepartmentName());
		//System.out.println(employee.getEmployeeID());
		return DBHandler.updateEmployee(employee);	   
	   }
	   
//	   public static void main(String[] args) {
//		   System.out.println(DBHandler.getUserByEmployeeId(105));
//		
//	}
}
