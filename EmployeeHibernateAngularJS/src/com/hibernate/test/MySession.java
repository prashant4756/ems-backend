package com.hibernate.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MySession {
	public Session getSession(){
		//creating configuration object  
		System.out.println("inside getSession method");
		Configuration cfg= new Configuration();
		cfg.configure("hibernate.cfg.xml"); //populates the data of the configuration file  
		
		//creating seession factory object  
		SessionFactory factory= cfg.buildSessionFactory();
		
		//creating session object  
		Session session= factory.openSession();
		return session;
	}
}
