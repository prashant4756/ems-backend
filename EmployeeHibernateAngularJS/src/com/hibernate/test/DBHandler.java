package com.hibernate.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DBHandler {
	public static void main(String[] args) {
		//deleteEmployee(106);
		//addEmployee("name8", "abcd dep", "govt emp" , "male");
		listEmployees();	
	}
	public static List<Employee> listEmployees() {
		List<Employee> employeeList=new ArrayList<Employee>();
		List<Employee> templist=null;
		Session session= new MySession().getSession();
		try{
			//creating transaction object  
			Transaction transaction= session.beginTransaction();
			Query query = session.createQuery("FROM Employee ORDER BY employeeid");
			//query.setMaxResults(2);
			
			templist = query.list();
//			System.out.println(templist.size());
//			for(int i=0 ; i < templist.size(); i++){
//				Employee tempEmployee = (Employee) templist.get(i);
//				System.out.print(tempEmployee.getEmployeeID()+" ");
//				System.out.print(tempEmployee.getEmployeeName()+" ");
//				System.out.print(tempEmployee.getDepartmentName()+" ");
//				System.out.print(tempEmployee.getProfession()+" ");
//				System.out.print(tempEmployee.getGender()+" ");
//				employeeList.add(tempEmployee);
//				System.out.println();
//			}
			transaction.commit();
				
		}catch (Exception e) {
			// TODO: handle exception
		}
		finally {
			session.close();
				
		}
		return templist;
		
	}
	public static List<Employee> addEmployee(Employee employee){
		Session session = new MySession().getSession();
		try{
			Transaction transaction= session.beginTransaction();
			session.persist(employee);
			transaction.commit();			
			System.out.println("successfully data added");
			
		}catch (Exception e) {
			// TODO: handle exception
		}finally {
			session.close();
		}
		return DBHandler.listEmployees();
	}
	
	public static void addEmployee(String employee_name, String department_name, String profession, String gender) {
		// TODO Auto-generated method stub
		Session session = new MySession().getSession();
		try{
			//creating transaction object  
			Transaction transaction= session.beginTransaction();
			
			Employee newEmp= new Employee();
			//newEmp.setEmployeeID(0);
			newEmp.setEmployeeName(employee_name);
			newEmp.setDepartmentName(department_name);
			newEmp.setProfession(profession);
			newEmp.setGender(gender);
			session.persist(newEmp);
			transaction.commit();			
			System.out.println("successfully data added");
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Unable to insert data --> "+e.getMessage());
		}
		finally {
			session.close();
			System.out.println();
		}
		
	}
	public static boolean updateEmployee(Employee employee){
		
		System.out.println("inside update employee method DBHandler: "+employee);
		boolean success=false;
		Session session= new MySession().getSession();
		Transaction transaction=null;
		try{
			transaction=session.beginTransaction();
			Employee tempEmployee= (Employee)session.get(Employee.class, employee.getEmployeeID());
			tempEmployee.setEmployeeName(employee.getEmployeeName());
			tempEmployee.setDepartmentName(employee.getDepartmentName());
			tempEmployee.setProfession(employee.getProfession());
			tempEmployee.setGender(employee.getGender());
			session.update(tempEmployee);
			
			transaction.commit();
			success=true;
			System.out.println("Data updated in database");		
		}catch (HibernateException e) {
			// TODO: handle exception
			success=false;
			if(transaction!=null){
				transaction.rollback();
			}
			System.out.println("hibernate exception: --> "+e.getMessage());
		}
		finally {
			session.close();
		}
		return success;
	}
	public static void updateEmployee(int employee_id, String employee_name){
		Session session= new MySession().getSession();
		Transaction transaction=null;
		try{
			transaction=session.beginTransaction();
			Employee employee= (Employee)session.get(Employee.class, employee_id);
			employee.setEmployeeName(employee_name);
			session.update(employee);
			
			transaction.commit();
			System.out.println("Data updated in database");		
		}catch (HibernateException e) {
			// TODO: handle exception
			if(transaction!=null){
				transaction.rollback();
			}
			System.out.println("hibernate exception: --> "+e.getMessage());
		}
		finally {
			session.close();
		}
	}
	public static boolean deleteEmployee(int employee_id) {
		// TODO Auto-generated method stub
		boolean success=false;
		Session session = new MySession().getSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			Employee employee =  session.get(Employee.class, employee_id);
			session.delete(employee);
			transaction.commit();
			success=true;
		}catch (Exception e) {
			// TODO: handle exception
			return success;
		}finally {
			session.close();
			
		}
		return success;
	}
	public static Employee getUserByEmployeeId(int employee_id) {
		// TODO Auto-generated method stub
		System.out.println("employee_id is "+employee_id);
		Employee employee = null;
		Session session = new MySession().getSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			//employee = session.get(Employee.class, employee_id);
			Query query = session.createQuery("FROM Employee where employeeid= "+employee_id);
			List<Employee> list= query.list();
			employee = list.get(0);
			transaction.commit();
		}catch (Exception e) {
			// TODO: handle exception
		}finally{
			session.close();
		}
		return employee;
	}
	
	
}

